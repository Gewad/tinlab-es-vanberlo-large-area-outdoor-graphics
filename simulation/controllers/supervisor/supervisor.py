# Copyright 1996-2020 Cyberbotics Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This controller gives to its node the following behavior:
Listen the keyboard. According to the pressed key, send a
message through an emitter or handle the position of PIXEL.
"""

from controller import Supervisor


class Driver (Supervisor):
    timeStep = 128
    x = 2.0
    z = -1.7
    translation = [x, 0.0, z]

    def __init__(self):
        super(Driver, self).__init__()
        self.receiver = self.getReceiver('receiver')
        self.emitter = self.getEmitter('emitter')
        robot = self.getFromDef('PIXEL')
        self.keyboard.enable(Driver.timeStep)
        self.keyboard = self.getKeyboard()

    def run(self):
        self.displayHelp()
        previous_message = ''

        # Main loop.
        while True:
            # Deal with the pressed keyboard key.
            k = self.keyboard.getKey()
            message = None
            if k == ord('Q'):
                message = ''


            # Send a new message through the emitter device.
            if k > 0 and message != None:
                print('Please, ' + message)
                self.emitter.send(message.encode('utf-8'))
                message = None

            # Perform a simulation step, quit the loop when
            # Webots is about to quit.
            if self.step(self.timeStep) == -1:
                break

    def displayHelp(self):
        print(
            'Commands:\n'
            ' none - should auto-start\n'
        )


controller = Driver()
controller.run()
