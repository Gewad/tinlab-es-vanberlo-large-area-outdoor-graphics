# Copyright 1996-2020 Cyberbotics Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This controller gives to its robot the following behavior:
According to the messages it receives, the robot change its
behavior.
"""

from controller import Robot
import math


class Enumerate(object):
    def __init__(self, names):
        for number, name in enumerate(names.split()):
            setattr(self, name, number)


class ChassisController:

    def __init__(self):
        self.maxSpeed = 2.9
        self.turnSpeed = 2.0
        self.status = Enumerate('STOP UTURN NEXT_ROW MOVE')
        self.doing = self.status.STOP
        self.motors = []
        self.wheelSpeeds = []
        self.chassisBot = Robot()
        self.timeStep = int(self.chassisBot.getBasicTimeStep())
        self.receiver = self.chassisBot.getReceiver('chassis in')
        self.emitter = self.chassisBot.getEmitter('chassis out')
        self.receiver.enable(self.timeStep)
        self.imu = self.chassisBot.getInertialUnit("inertial unit")
        self.imu.enable(self.timeStep)
        self.firstColumn = True

        # row counters (do not change)
        self.amountOfRows = 0 # how many rows are in the next column
        self.rowCount = 0 # current amount of rows done
        self.rowStepCount = 0 # current amount of steps taken (do not change)

        # add motors to motors list
        self.motors.append(self.chassisBot.getMotor("wheel front left"))
        self.motors.append(self.chassisBot.getMotor("wheel front right"))
        self.motors.append(self.chassisBot.getMotor("wheel rear left"))
        self.motors.append(self.chassisBot.getMotor("wheel rear right"))

        self.NORTH = 1.0
        self.SOUTH = 0.0
        self.direction = self.NORTH

        # initialise motors to stopped
        for motor in self.motors:
            motor.setPosition(float("inf"))
            motor.setVelocity(0.0)
            self.wheelSpeeds.append(0.0)

    def boundSpeed(self, speed):
        return max(-self.maxSpeed, min(self.maxSpeed, speed))

    def moveForward(self):
        for i in range(len(self.wheelSpeeds)):
            self.wheelSpeeds[i] = self.maxSpeed

    def turnLeft(self):
        self.wheelSpeeds[0] = self.maxSpeed / 2
        self.wheelSpeeds[1] = self.maxSpeed
        self.wheelSpeeds[2] = self.maxSpeed / 2
        self.wheelSpeeds[3] = self.maxSpeed

    def turnRight(self):
        self.wheelSpeeds[0] = self.maxSpeed
        self.wheelSpeeds[1] = self.maxSpeed / 2
        self.wheelSpeeds[2] = self.maxSpeed
        self.wheelSpeeds[3] = self.maxSpeed / 2

    def uturnDecide(self):
        if(self.direction == self.NORTH):
            self.uturnRight()
        elif(self.direction == self.SOUTH):
            self.uturnLeft()
        else:
            print('Nothing')

    def uturnRight(self):
        if (self.checkTurn()):
            self.doing = self.status.MOVE
            self.direction = self.SOUTH
        else:
            self.wheelSpeeds[0] = self.turnSpeed
            self.wheelSpeeds[1] = - self.turnSpeed / 5
            self.wheelSpeeds[2] = self.turnSpeed
            self.wheelSpeeds[3] = - 0.12

    def uturnLeft(self):
        if (self.checkTurn()):
            self.doing = self.status.MOVE
            self.direction = self.NORTH
        else:
            self.wheelSpeeds[0] = - self.turnSpeed / 5
            self.wheelSpeeds[1] = self.turnSpeed
            self.wheelSpeeds[2] = - 0.04
            self.wheelSpeeds[3] = self.turnSpeed

    def moveSteps(self, steps):
        # check if enough steps have been made
        if steps == self.rowStepCount:
            self.stop()
            self.rowStepCount = 0
        else:
            # not there yet, keep moving forward
            self.moveForward()
            self.rowStepCount += 1

    def stop(self):
        self.doing = self.status.STOP
        for i in range(len(self.wheelSpeeds)):
            self.wheelSpeeds[i] = 0.0
        self.emitter.send(b'paint/moveDone') # done moving

    def readIMU(self):
        rollPitchYaw = self.imu.getRollPitchYaw()
        (_, _, yaw) = rollPitchYaw  # value is +/- pi
        yaw = round((yaw / math.pi), 2)  # round to n decimal places
        # print("DEBUG yaw: ", yaw)
        return yaw

    # check the position on track if car is trailing return false
    # if car is going straight return true
    def checkForFault(self):
        if self.direction == self.NORTH:
            if self.readIMU() != 1.0:
                return True
            else:
                return False
        elif self.direction == self.SOUTH:
            if self.readIMU() != 0.0:
                return True
            else:
                return False
        else:
            print('oops heading not initialized')

    # check if the robot turns right return true if on correct position
    def checkTurn(self):
        if self.direction == self.NORTH:
            if abs(self.readIMU()) == self.SOUTH:
                return True
            else:
                return False
        elif self.direction == self.SOUTH:
            if abs(self.readIMU()) == self.NORTH:
                return True
            else:
                return False
        else:
            print('oops heading is not initialized')

    def run(self):
        while True:
            self.readIMU()
            if self.receiver.getQueueLength() > 0:
                #Recieve and decode message
                message = self.receiver.getData().decode('utf-8')
                self.receiver.nextPacket()
                command = message.split("/")
                #Check messages on the movement "topic"

                if command[0] == 'movement':
                    if command[1] == "moveAmount":
                        if self.amountOfRows == 0:
                            if self.firstColumn == True:
                                # Only first column
                                self.amountOfRows = int(command[2]) - 1
                                self.firstColumn = False
                                self.emitter.send(b'paint/moveDone')
                            else:
                                # All other columns
                                self.amountOfRows = int(command[2]) - 1
                                self.doing = self.status.UTURN
                                self.rowCount = 0
                    elif command[1] == "paintDone":
                        if self.amountOfRows == self.rowCount:
                            self.emitter.send(b'pixel/amountDone')
                            self.amountOfRows = 0
                            # End of column, wait for new column
                        else:
                            # Go to next row and message when done moving
                            self.doing = self.status.NEXT_ROW
                            self.rowCount += 1

            if self.doing == self.status.NEXT_ROW:
                self.moveSteps(12)
            elif self.doing == self.status.MOVE:
                self.moveSteps(30)
            elif self.doing == self.status.UTURN:
                self.uturnDecide()

            # set motors to speeds listed in wheelSpeeds
            for i in range(len(self.motors)):
                self.motors[i].setVelocity(self.wheelSpeeds[i])

            # Webots is about to quit.
            if self.chassisBot.step(self.timeStep) == -1:
                break

controller=ChassisController()
controller.run()