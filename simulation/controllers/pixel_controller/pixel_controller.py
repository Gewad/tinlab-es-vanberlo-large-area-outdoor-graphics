# Copyright 1996-2020 Cyberbotics Ltd.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""
This controller gives to its robot the following behavior:
According to the messages it receives, the robot change its
behavior.
"""

from controller import Robot

class Enumerate(object):
    def __init__(self, names):
        for number, name in enumerate(names.split()):
            setattr(self, name, number)

class Raspberry(Robot):
    def boundSpeed(self, speed):
        return max(-self.maxSpeed, min(self.maxSpeed, speed))

    def __init__(self):
        self.simBot = Robot()
        self.timeStep = int(self.simBot.getBasicTimeStep())
        self.receiver = self.simBot.getReceiver('raspi in')
        self.emitter = self.simBot.getEmitter('raspi out')
        self.receiver.enable(self.timeStep)

    def run(self):
        # This is the API that handles the bitmap image for the paint_controller
        # and the moveAmount (amount of rows per column) for the chassis_controller

        # Start sim code (initilize phase):
        self.emitter.send(b'movement/moveAmount/30')
        # TODO: send bitmap message to paint_controller (supposed to receive from MQTT)

        while True:
            # Read incomming messages
            if self.receiver.getQueueLength() > 0:
                message = self.receiver.getData().decode('utf-8')
                self.receiver.nextPacket()
                if message == 'pixel/amountDone':
                    # Reached end of column, send next column
                    self.emitter.send(b'movement/moveAmount/30')


            # Perform a simulation step, quit the loop when Webots is about to quit.
            if self.simBot.step(self.timeStep) == -1:
                break

controller = Raspberry()
controller.run()
