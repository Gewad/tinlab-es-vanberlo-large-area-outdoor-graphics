from controller import Robot
import json

NOZZLE_AMOUNT = 5

HARD_CODED_BITMAP = True

class PaintController:
    def __init__(self):
        if HARD_CODED_BITMAP:
            # STICK MAN FIGURE:
            # self.bitmap = [
            #     [0,1,1,1,0],[1,0,0,0,1],[1,0,0,0,1],[1,0,0,0,1],[0,1,1,1,0],
            #     [0,0,1,0,0],[1,0,1,0,1],[1,1,1,1,1],[0,1,1,1,0],[0,0,1,0,0],
            #     [0,1,1,1,0],[1,1,0,1,1],[0,1,1,1,0],[1,0,0,0,1],[1,0,0,0,1],
            #     [1,0,0,0,1],[0,1,1,1,0],[0,0,1,0,0],[1,0,1,0,1],[1,1,1,1,1],
            #     [0,1,1,1,0],[0,0,1,0,0],[0,1,1,1,0],[1,1,0,1,1],[0,1,1,1,0],
            #     [1,0,0,0,1],[1,0,0,0,1],[1,0,0,0,1],[0,1,1,1,0],[0,0,1,0,0],
            #     [1,0,1,0,1],[1,1,1,1,1],[0,1,1,1,0],[0,0,1,0,0],[0,1,1,1,0],[1,1,0,1,1]
            # ]

            # VANBERLO logo:
            self.bitmap =  [[0,0,0,0,0],[0,0,1,1,1],[0,0,1,1,1],[0,0,0,1,1],[0,0,0,1,1],[0,0,0,1,1],
                            [0,0,0,1,1],[0,0,0,1,1],[0,0,0,1,1],[0,0,0,0,1],[0,0,0,0,1],[0,0,0,0,1],
                            [0,0,1,1,1],[0,0,1,1,1],[0,0,1,1,1],[0,0,1,1,1],[0,0,0,1,1],[0,0,0,1,1],
                            [0,0,0,0,1],[0,0,0,0,1],[0,0,0,1,1],[0,0,0,1,1],[0,0,0,1,1],[0,0,0,1,1],
                            [0,0,0,1,1],[0,0,0,1,1],[0,0,0,1,1],[0,0,0,0,1],[0,0,0,0,1],[0,0,0,0,0],
                            [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,1],[0,0,0,1,1],
                            [0,0,1,1,1],[0,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[0,1,1,1,1],[1,1,1,1,1],
                            [1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],
                            [1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,1,1],[1,1,1,0,1],[1,1,0,0,1],
                            [1,0,0,0,0],[1,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
                            [0,0,0,0,0],[1,1,0,0,0],[1,0,0,0,0],[1,0,0,0,0],[1,0,0,0,0],[1,0,0,0,0],
                            [1,0,0,0,0],[1,0,0,0,0],[1,0,0,0,0],[1,0,0,0,0],[1,0,0,0,0],[1,0,0,0,0],
                            [1,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[1,1,0,0,0],[1,1,1,0,0],
                            [1,1,1,1,1],[0,0,0,1,1],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,1,1],
                            [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
                            [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,1,1,0,0],[0,0,1,1,1],
                            [0,0,1,1,1],[0,0,0,0,0],[0,0,0,1,0],[0,0,1,1,1],[0,0,0,0,1],[0,0,0,0,0],
                            [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
                            [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],
                            [0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0],[0,0,0,0,0]]
        else:
            self.bitmap = []
        self.nozzles = []
        self.nozzToggle = []
        self.paintBot = Robot()
        self.timeStep = int(self.paintBot.getBasicTimeStep())

        self.receiver = self.paintBot.getReceiver('paint in')
        self.emitter = self.paintBot.getEmitter('paint out')
        self.receiver.enable(self.timeStep)

        self.setColorAll(0xFFFFFF)

        #add pens to nozzles list
        for i in range(NOZZLE_AMOUNT):
            self.nozzles.append(self.paintBot.getPen(f"pen{i}"))
            self.nozzToggle.append(False)

    def toggleNozzle(self, i):
        val = self.nozzToggle[i-1]
        val = not val #toggle boolean
        self.nozzToggle[i-1] = val

    def setIndividualColor(self, nozzleIndex, rgbHex):
        '''
        0xRRGGBB hexadecimal format (i.e., 0x000000 is black
        0xFF0000 is red, 0x00FF00 is green, 0x0000FF is blue,
        0xFFA500 is orange, 0x808080 is gray 0xFFFFFF is white, etc.).
        '''
        self.nozzles[nozzleIndex].setInkColor(rgbHex, 1)

    def setColorAll(self, rgbHex):
        '''
        0xRRGGBB hexadecimal format (i.e., 0x000000 is black
        0xFF0000 is red, 0x00FF00 is green, 0x0000FF is blue,
        0xFFA500 is orange, 0x808080 is gray 0xFFFFFF is white, etc.).
        '''
        for i in range(len(self.nozzles)):
            self.nozzles[i].setInkColor(rgbHex, 1)

    def paintLine(self):
        #pop the first list of the bitmaplist to paint
        if self.bitmap:
            line = self.bitmap.pop(0)

            #convert the list with 0 and 1 into boolean list
            line = list(map(bool,line))

            #use this list to actually paint
            self.paint(line)

            #Send paintDone message to movement
            message = (b'movement/paintDone')
            self.emitter.send(message)

    def paint(self, line):
        #initiate the nozzles that should paint
        for nozzle, pixel in zip(self.nozzles, line):
            nozzle.write(pixel)

    def setBitmap(self, bitmapString):
        #Convert message a dict and take the value key as the bitmapList
        self.bitmap = json.loads(bitmapString)["bitmap"]
        pass

    def run(self):
        while True:
            #reset all nozzles to not paint
            for nozzle in self.nozzles:
                nozzle.write(False)

            if self.receiver.getQueueLength() > 0:
                #Recieve and decode message
                message = self.receiver.getData().decode('utf-8')
                self.receiver.nextPacket()
                command = message.split("/")
                #Check messages on the paint "topic"
                if command[0] == 'paint':
                    if command[1] == "moveDone":
                        self.paintLine()
                    elif command[1] == "paintMap":
                        self.setBitmap(command[2])

            # Webots is about to quit.
            if self.paintBot.step(self.timeStep) == -1:
                break


controller = PaintController()
controller.run()
