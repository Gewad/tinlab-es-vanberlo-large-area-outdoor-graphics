# Webots Simulation User Manual

## Installation
Make sure the following is already installed on your system.
> As of writing (July 2020), Webots is currently only compatable with Python 3.7. Be sure to install this version to avoid complications. 
- Git (https://git-scm.com/book/en/v2/Getting-Started-Installing-Git, or via your favorite package manager/repository)
- Python 3.7 ( https://www.python.org/downloads/release/python-378/)

Then download the Webots package (as of writing, the latest version is R2020a-rev1, released 14 Jan 2020)
- Webots R2020a-rev1 (https://cyberbotics.com/#download)
- See https://cyberbotics.com/doc/guide/installation-procedure for further instructions.

Using your favorite bash terminal (eg. Git BASH), use the following commands to clone the repository and open the simulation.
```sh
$ cd /path/to/repo/directory
$ git clone https://gitlab.com/Gewad/tinlab-es-vanberlo-large-area-outdoor-graphics.git
```

## Opening the simulation
>Before using Webots, it is recommended to follow the tutorials provided by Cyberbotics to order to familiarise yourself with Webots. See https://cyberbotics.com/doc/guide/tutorials and https://cyberbotics.com/doc/guide/getting-started-with-webots

Navigate to and open `/simulation/worlds/Pixel.wbt`. Webots should open the simulation.
Alternatively, open Webots and open the Pixel.wbt worlds via `File > Open World...` and navigating to the `Pixel.wbt` file located in `simulation/worlds/`

The simulation should start automatically. If not, press the `Run simulation` button

### Editing controllers
>As mentioned in the Webots Tutorials, always reset the simulation before making any changes to the robot and/or world, as these changes will be lost whenever the world is reloaded.
In the Scene Tree on the left, expand `Pixel`. The first 3 field are the controller files assigned to the three components of the robots, namely
- `raspi_controller.py`, a simulation of the process on a Raspberry Pi that runs the communication and interpretation of the image conversion
- `paint_controller.py`, a simulation of the script that runs on a Raspberry Pi that runs the script used to control the PWM board and servo motors attached
- `chassis_controller.py`, a simulation of the script that runs the stepper motors and other sensors on the robot.

By selecting one of these fields, you can click the `Select...` button to assign another file, or `Edit` to edit the file within the Webots Text Editor.
>It's also possible to edit these files with your favorite editor outside of Webots. If you do this, be sure to set your editor to insert 4 spaces when using indents/tabs and to save any changes in the editor before returning to Webots.

The `Supervisor` node in the Scene Tree is designed to handle certain keyboard presses. To edit this, expand the `Supervisor` node in the Scene Tree, select `controller` and edit the file.

### Running the simulation
Pressing the `Run Simulation` button will run the simulation. Any print statements, errors and/or warnings will appear in the console in Webots, preceding with which controller it originated from. 

### Selecting another image to paint
As communication between the image conversion system and the simulation has not yet been implemented, the bitmap Boolean array has been hard coded in `paint_controller.py`. Currently, there are two hard coded images available, the VanBerlo logo and a Stickman figure.

To select the stickman figure, uncomment lines 12 - 20, and comment lines 23 - 42.
To select the VanBerlo logo, uncomment lines 23 - 42 and comment lines 12  - 20.
