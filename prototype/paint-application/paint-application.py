import paho.mqtt.client as mqtt
import logging
import servo_pwm as servo
import time

bitmapList = []

def error(error):
    client.publish("error", error, 1)

def newLine(message):

    global bitmapList

    #There are items in the bitmaplist to paint
    if bitmapList:
        line = bitmapList.pop(0)
        for i, pixel in enumerate(line):
            #assume the contents of Pixel is a bool
            #use library to start the spray of each pixel if needed
            servo.spray(pixel, i)

        #wait 1 second for cans to spray
        endMillis = int(round(time.time() * 1000)) + 1000
        while int(round(time.time() * 1000)) < endMillis:
            pass

        for i, pixel in enumerate(line):
            #turn of all the sprays
            servo.spray(False, i)

        #Done painting, move to next row
        client.publish("paintDone", "done", 1)

    else:
        #Error:
        #The program should not get a paint job
        #while it has an empty bitmapList
        pass

def paintMap(message):
    global bitmapList
    bitmapList = eval(message.payload.decode('utf-8'))["map"]

callbackList = {
    'moveDone': newLine,
    'paintMap': paintMap,
    'error': error,
}

def on_connect(client, userdata, flags, rc):
    #Subscribe to all the topics in callbackList
    for topic in callbackList:
      client.subscribe(topic)

def ondisconnect(client, userdata, rc):
    pass

def onsubscribe(client, userdata, mid, granted_qos):
    pass

def onmessage(client, userdata, msg):
    #logging.debug("incomming message")
    callbackList[msg.topic](msg)


if __name__ == "__main__":
    try:
        logging.basicConfig(filename='verfMQTT.log', level=logging.DEBUG)
        client = mqtt.Client()
        client.on_connect= on_connect
        client.on_message = onmessage
        client.on_disconnect=ondisconnect
        client.on_subscribe=onsubscribe

        client.connect("localhost", 1883, 60)
        client.loop_forever()
    except Exception as e:
        logging.critical(e)
