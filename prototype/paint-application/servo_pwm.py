from adafruit_servokit import ServoKit
import time

SPRAY_DEGREE = 180
REST_DEGREE = 90
kit = ServoKit(channels=16)

def spray(shouldSpray, servoIndex):
  if shouldSpray == True:
    kit.servo[servoIndex].angle = SPRAY_DEGREE
  else:
    kit.servo[servoIndex].angle = REST_DEGREE
