#include "BasicStepperDriver.h"

// 1 step is 4 degrees
// 90 times is a full rotation (360 degrees)

#define MOTOR_STEPS 200
#define RPM 180
#define MICROSTEPS 1

// Single motor test setup
#define LED A0

// Chasis robot setup
#define DIR_RIGHT 7
#define STEP_RIGHT 6
#define DIR_LEFT 9
#define STEP_LEFT 8

BasicStepperDriver right_stepper(MOTOR_STEPS, DIR_RIGHT, STEP_RIGHT);
BasicStepperDriver left_stepper(MOTOR_STEPS, DIR_LEFT, STEP_LEFT);

// Starts at -1 so if paintDone is called without first sending a row amount
// returns illegal instead of actually moving.
int movesLeft = -1;


void setup() {
  Serial.begin(9600);
  pinMode(LED, OUTPUT);
  // stepper.setEnableActiveState(LOW);
  right_stepper.begin(RPM, MICROSTEPS);
  left_stepper.begin(RPM, MICROSTEPS);
  delay(500);
}

void loop() {
  // Wait for a Serial message to be available.
  if (Serial.available() > 0) {
    // Read the message until a newline is reached. Then decode this into a String.
    String message = Serial.readString();

    
    if (message == "paintDone\n") { // paintDone is a signal that we should be on the move again.
      // paintDone without any moves left is illegal. This can also be caused by not having a set movesleft.
      if (movesLeft < 0) {
        blink(3);
        Serial.println("illegal");
        return;
      }

      // If movesLeft is 0 the Robot should turn to be available for the next row. Else it can move forward.
      if (movesLeft-- == 0) {
        turnRight();
        // Indicates to the API the Robot is done with the current Collumn.
        Serial.println("amountDone");
      } else {
        stepForward();
        // Indicates to the API the robot is done moving and it can paint again.
        Serial.println("moveDone");
      }
    } else {
      // Five blinks to indicate moveAmount received.
      blink(5)

      // Convert message to an Integer.
      movesLeft = message.toInt();
    }
  }
}

void stepForward() {
  // One blink to indicate move.
  blink(1);
  for (int i = 0; i <= 90; i++)
  {
    // Steps forward a couple of degrees at a time with a small delay to increase consistency.
    right_stepper.rotate(4);
    left_stepper.rotate(-4);
    delayMicroseconds(10);
  }
}

void turnRight() {
  // Two blinks to indicate turn.
  blink(2);
  for (int i = 0; i <= 45; i++) {
    left_stepper.rotate(-4);
  }
}

void blink(int amount) {
  for (int i = 0; i < amount; i++) {
    digitalWrite(LED, HIGH);
    delay(100);
    digitalWrite(LED, LOW);
    delay(100);
  }
}
