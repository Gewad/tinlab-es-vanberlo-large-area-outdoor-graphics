import paho.mqtt.client as mqtt
import logging
import serial
import json


currentBitMap = []
errorInSystem = False

def loop():
    while True:
        # Check if there is a message.
        message = ser.read_until().decode('utf-8')
        # If there is a message remove all the newlines and spaces.
        if message:
            message = message.rstrip()
        if message == 'moveDone':
            moveDone()
            
        # If nothing was received with Serial the normal MQTT loop runs.
        client.loop()
        

def moveDone():
    client.publish('moveDone', 1, 1)

def paintDone(msg):
    ser.write('paintDone\n'.encode('utf-8'))

def bitmap(message):
    currentBitMap = eval(message.payload.decode('utf-8'))['map']
    print(currentBitMap)
    
    # Expendability: Properly implement multiple collumns.
    paintMap = currentBitMap
    
    # Send the map to the paint application
    client.publish("paintMap", json.dumps({"map":paintMap}), 1)
    # Send moveDone at the beginning so the paint application starts painting
    client.publish('moveDone', 1, 1)
    # Write the amount of rows to the Arduino so it knows when to turn.
    ser.write(str(len(paintMap)).encode('utf-8'))

def amountDone(message):
    if not errorInSystem:
        # Gets the next five rows for all of the collumns
        paintMap = [[i.pop(y) for y in range(0, 5)] for i in currentBitMap]
        client.publish("paintMap", paintMap, 1)
        ser.write(bytes(len(paintMap)))

def error(message):
    errorInSystem = True
    logging.error("Fatal error in system, stopped")

callbackList = {
    'bitmap': bitmap,
    'amountDone': amountDone,
    'error': error,
    'paintDone': paintDone
}

def onconnect(client, userdata, flags, rc):
    logging.info("connected to broker")
    [client.subscribe(topic) for topic in callbackList]

def ondisconnect(client, userdata, rc):
    logging.info("disconnected broker")

def onsubscribe(client, userdata, mid, granted_qos):
    logging.info("subscribed to broker")
    
def onmessage(client, userdata, msg):
    callbackList[msg.topic](msg)


if __name__ == "__main__":
    try:
        logging.basicConfig(filename='./temp/log/mqttBroker.log',level=logging.DEBUG)
        
        # Set up Serial communication, note timeout is 0  to make the read_until() function non-blocking.
        ser = serial.Serial("/dev/ttyACM0", 9600,timeout=0)
        ser.baudrate = 9600

        client = mqtt.Client()
        client.on_connect=onconnect
        client.on_message = onmessage
        client.on_disconnect=ondisconnect
        client.on_subscribe=onsubscribe

        client.connect("localhost", 1883, 60)
        loop()
    except Exception as e:
        logging.critical("Could not connect to broker: " + e.__class__.__name__)