package nl.projectvanberlo.image_processing.exception;

public class FileConversionException extends RuntimeException{
    public FileConversionException(String message){
        super(message);
    }
}
