package nl.projectvanberlo.image_processing.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {
    /**
     * Initialise the StorageService by creating the files directory
     */
    void init();

    /**
     * Store a file to the disk
     * @param file the file to be stored
     */
    void store(MultipartFile file);

    /**
     * Load all file paths
     * @return A stream with file paths
     */
    Stream<Path> loadAll();

    /**
     * Load a specific file
     * @param filename The name of the file to be loaded
     * @return The path to the file
     */
    Path load(String filename);

    /**
     * LOad a file as a Resource
     * @param filename The name of the file to be loaded
     * @return The Resource of the file
     */
    Resource loadAsResource(String filename);

    /**
     * Delete all files available to the webserver
     */
    void deleteAll();
}
