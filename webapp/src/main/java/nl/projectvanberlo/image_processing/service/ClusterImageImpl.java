package nl.projectvanberlo.image_processing.service;


import nl.projectvanberlo.image_processing.property.ClusterImageProperties;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.awt.image.BufferedImage;
import java.awt.image.AffineTransformOp;
import java.awt.geom.AffineTransform;
import javax.imageio.ImageIO;
import java.lang.Math;

@Slf4j
@Service
public class ClusterImageImpl implements ClusterImage{

    private final int clusterSizeDevice;

    @Autowired
    public ClusterImageImpl(ClusterImageProperties properties) {
        this.clusterSizeDevice = properties.getClusterSize();
    }

    @Override
    public void cluster(String oP){
        log.info("Starting image clustering");

        //Reading the bmp image in
        BufferedImage image;
        try {
            image = getBMPImage(oP);
        }
        catch(IOException e) {
            log.info("Could not open image");
            e.printStackTrace();
            return;
        }

        //Setting the resolution of the bmp to a multiple of clusterSizeDevice
        image = clusterBufferedImage(image);

        //Writing the image to a file.
        if(writeBufferedImageToBMP(oP, image)) {
            log.info("Successfully clustered image");
        }

    }

    @Override
    public BufferedImage clusterBufferedImage(BufferedImage image) {
        int[] res = getResForCluster(image, clusterSizeDevice, 0);
        image = ResizeBMP(image,res[0],res[1]);

        return image;
    }

    @Override
    public BufferedImage clusterBufferedImage(BufferedImage image, int maxRows) {
        int[] res = getResForCluster(image, clusterSizeDevice, maxRows);
        image = ResizeBMP(image,res[0],res[1]);

        return image;
    }

    private int[] getResForCluster(BufferedImage image, int clustersize, int maxRows) {
        //Get current resolution
        int startWidth = image.getWidth();
        int startHeight = image.getHeight();

        //Getting the resolution as close as possible to
        //the original resolution while still being a multiple of clustersize.
        int newWidth;
        if(maxRows > 0) {
            newWidth = (int)Math.round((float)startWidth / (float)clustersize) * clustersize;
            //If the newWidth is larger than the max width set newWidth to the maxWidth.
            if ((newWidth/clustersize) > maxRows) {
                newWidth = clustersize * maxRows;
            }
        }
        else {
            newWidth = (int)Math.round((float)startWidth / (float)clustersize) * clustersize;
        }

        //Getting the scale at which width has changed
        float widthScale = (float)newWidth / (float)startWidth;

        //Make the image stay at the name scale as before.
        //This does not need to be a multiple of clustersize.
        int newHeight = (int)Math.round((float)startHeight * widthScale);

        int[] res = {newWidth, newHeight};
        return res;
    }

    private BufferedImage ResizeBMP(BufferedImage imageIn, int newWidth, int newHeight) {
        //Get current resolution
        int startWidth = imageIn.getWidth();
        int startHeight = imageIn.getHeight();

        //Creating scale to scale the image with.
        float widthScale = (float)newWidth / (float)startWidth;
        float heightScale = (float)newHeight / (float)startHeight;

        //Make a new bmp image with new resolution
        BufferedImage imageOut = new BufferedImage(newWidth, newHeight, imageIn.getType());


        // Code extract created with help from Stack Overflow
        // Question User:	https://stackoverflow.com/users/52924/thiago-diniz
        // Question: 		https://stackoverflow.com/questions/4216123/how-to-scale-a-bufferedimage
        // Answer User: 	trashgod https://stackoverflow.com/users/230513/trashgod
        // Answer:			https://stackoverflow.com/a/4216635

        AffineTransform at = new AffineTransform();
        at.scale(widthScale, heightScale);
        //Bicubic interpolation chosen so that the image will be smoother looking since the image will be semi-permanent.
        AffineTransformOp scaleOp = new AffineTransformOp(at, AffineTransformOp.TYPE_BICUBIC);
        //Convert to new resolution
        imageOut = scaleOp.filter(imageIn, imageOut);

        // End code extract from Stack Overflow

        //Return the image
        return imageOut;

    }

    private BufferedImage getBMPImage(String bmLocation) throws IOException {
        File bmpFileIn;
        BufferedImage bmpImageIn;

        try {
            bmpFileIn = new File(bmLocation);
            bmpImageIn = ImageIO.read(bmpFileIn);
        }
        catch(IOException e) {
            throw e;
        }
        return bmpImageIn;
    }

    private boolean writeBufferedImageToBMP(String bmLocation, BufferedImage image) {
        try {
            File bmpFileOut = new File(bmLocation);
            ImageIO.write(image, "bmp", bmpFileOut);
        }
        catch(IOException e) {
            return false;
        }
        return true;
    }

}
