package nl.projectvanberlo.image_processing.service;

import lombok.extern.slf4j.Slf4j;
import nl.projectvanberlo.image_processing.property.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@Slf4j
@Service
public class ImageConversionImpl implements ImageConversion{
    private final BMPConversion bmpConversion;
    private final MonochromeConversion monochromeConversion;
    private final ClusterImage clusterImage;
    private final ArrayConversion arrayConversion;
    private final Path rootLocation;

    @Autowired
    private ImageConversionImpl(BMPConversion bmpConversion, MonochromeConversion monochromeConversion, ClusterImage clusterImage, ArrayConversion arrayConversion, StorageProperties properties){
        this.bmpConversion = bmpConversion;
        this.monochromeConversion = monochromeConversion;
        this.clusterImage = clusterImage;
        this.arrayConversion = arrayConversion;
        this.rootLocation = Paths.get(properties.getLocation());
    }

    @Override
    public boolean[][] convert(String filename) throws IOException {
        String inputPath = this.rootLocation.resolve(filename).toString();
        String outputPath = this.rootLocation.resolve(getOutputFileName(filename)).toString();

        bmpConversion.convert(inputPath, outputPath); //writes .bmp file

        monochromeConversion.convert(outputPath); //for now, outputPath == inputPath, overwrites file

        clusterImage.cluster(outputPath);
        return arrayConversion.convert(outputPath);
    }

    @Override
    public String getOutputFileName(String inputFilename) {
        log.info("InputFile " + inputFilename);
        String[] fileSplit = inputFilename.split("\\.");
        return fileSplit[0] + "_output.bmp";
    }
}
