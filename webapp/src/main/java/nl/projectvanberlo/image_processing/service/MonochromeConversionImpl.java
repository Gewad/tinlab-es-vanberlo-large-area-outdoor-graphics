package nl.projectvanberlo.image_processing.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class MonochromeConversionImpl implements MonochromeConversion{
    @Override
    public void convert(String oP){

        try {
            log.info("Starting .bmp conversion to its monochrome version");

            File input = new File(oP);
            BufferedImage image = ImageIO.read(input);

            BufferedImage result = toMonochrome(image, 125);

            File output = new File(oP);
            ImageIO.write(result, "bmp", output);

            log.info("Successfully converted .bmp to a monochrome version");

        } catch (IOException ioe) {
            log.error("Couldn't read file, invalid file format.");

        } catch (NullPointerException npe) {
            log.error("Unknown source, file format not applicable for conversion.");

        } catch (Exception e) {
            log.error("Unknown error by conversion of input file to bmp format");
            e.printStackTrace();

        }
    }

    private BufferedImage toMonochrome(BufferedImage image, int threshold){
        //Create a new buffered image with a binary mode since there will only be two colors stored.
        BufferedImage result = new BufferedImage(
                image.getWidth(),
                image.getHeight(),
                BufferedImage.TYPE_BYTE_BINARY);

        int white = (new Color(255,255,255)).getRGB();
        int black = (new Color(0,0,0)).getRGB();

        //Going through each pixel
        int rgb,red,green,blue;
        for (int heightIter = 0; heightIter < image.getHeight(); heightIter++){
            for (int widthIter = 0; widthIter < image.getWidth(); widthIter++){
                //getting the rgb values
                rgb = image.getRGB(widthIter, heightIter);
                red = (rgb & 0x00ff0000) >> 16;
                green = (rgb & 0x0000ff00) >> 8;
                blue  =  rgb & 0x000000ff;
                
                //check if the rgb value go above the set threshold
                if(red <= threshold || green <= threshold || blue <= threshold ){
                    result.setRGB(widthIter,heightIter,black);
                }
                else {
                    result.setRGB(widthIter,heightIter,white);
                }
            }
        }
        return result;
    }

}
