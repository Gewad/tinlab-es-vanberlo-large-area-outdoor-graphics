package nl.projectvanberlo.image_processing.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import nl.projectvanberlo.image_processing.service.MqttService;
import nl.projectvanberlo.image_processing.exception.StorageException;
import nl.projectvanberlo.image_processing.exception.StorageFileNotFoundException;
import nl.projectvanberlo.image_processing.property.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

@Service
@Slf4j
public class StorageServiceImpl implements StorageService{

    private final MqttService mqttService;
    private final Path rootLocation;
    private final int maxUploadSize;
    private final ImageConversion imageConversion;

    @Autowired
    public StorageServiceImpl(StorageProperties properties, ImageConversion imageConversion, MqttService mqttService) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.maxUploadSize = properties.getMaxUploadSize();
        this.imageConversion = imageConversion;
        this.mqttService = mqttService;
    }

    @Override
    public void init() {
        try{
            Files.createDirectories((rootLocation));
        }catch(IOException e){
            throw new StorageException("Could not initialize storage", e);
        }
    }

    @Override
    public void store(MultipartFile file) {
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try{
            if (file.isEmpty()){
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")){
                throw new StorageException("Cannot store file with relative path outside current directory " + filename);
            }
            if (!file.getContentType().contains("image/")){
                throw new StorageException(
                        "Cannot store file with this content type" + file.getContentType()
                );
            }
            if (file.getSize() > maxUploadSize){
                throw new StorageException(
                        "Cannot store store a file with the size of " + file.getSize()
                );
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(filename), StandardCopyOption.REPLACE_EXISTING);
                log.info(this.rootLocation.resolve(filename).toString());
                mqttService.sendBitmap(imageConversion.convert(filename));
                imageConversion.getOutputFileName(filename);
//                log.info("Output file: " + imageConversion.getOutputPath(this.rootLocation.resolve(filename).toString()));
            }
        }catch (IOException e){
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    @Override
    public Stream<Path> loadAll() {
        try{
            return Files.walk(this.rootLocation, 1)
            .filter(path -> !path.equals(this.rootLocation))
            .map(this.rootLocation::relativize);
        }catch (IOException e){
            throw new StorageException("Failed to read stored files", e);
        }
    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try{
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()){
                return resource;
            }else{
                throw new StorageFileNotFoundException("Could not read file: " + filename);
            }
        }catch(MalformedURLException e){
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }
}
