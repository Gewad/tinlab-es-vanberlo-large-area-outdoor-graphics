package nl.projectvanberlo.image_processing.service;

public interface ArrayConversion {
    /**
     * Creates the instruction map now that the image has been converted
     *
     * Since this function ONLY returns true when a pixel is fully black (Hex 000000),
     * it is nessecary to run the monochromeConversion first to not miss any pixels
     *
     * @param iP inputPath, used to read the converted image
     * @return boolean instruction map
     */
    boolean[][] convert(String iP);
}
