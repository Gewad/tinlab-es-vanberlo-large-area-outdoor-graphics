package nl.projectvanberlo.image_processing.service;

import nl.projectvanberlo.image_processing.property.MqttProperties;
import nl.projectvanberlo.image_processing.service.MqttService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;
import org.json.JSONObject;

@Service
public class MqttServiceImpl implements MqttService{
	
	private final String topic;
	private final int qos;
	private final String broker;
	private final String clientId;
	
	static private MemoryPersistence persistence = new MemoryPersistence();
	static private MqttClient mqttClient;

	@Autowired
    public MqttServiceImpl(MqttProperties properties) {
        this.clientId = properties.getClientId();
        this.broker = properties.getBroker();
        this.topic = properties.getTopic();
        this.qos = properties.getQos();
		connect();
    }
	
	@Override
	public boolean sendBitmap(boolean[][] bitmap) {
		if (!mqttClient.isConnected()) {
			if(!connect()) {
				return false;
			}
		}
		if(!publish(arrayToJSONStr(bitmap))) {
			return false;
		}
		return true;
	}
	
	@Override
	public boolean connect() {
		try {
			mqttClient = new MqttClient(broker, clientId, persistence);
	        MqttConnectOptions connOpts = new MqttConnectOptions();
	        connOpts.setCleanSession(true);
	        mqttClient.connect(connOpts);
	        return true;
		}
		catch(MqttException me) {
			me.printStackTrace();
			return false;
		}
	}
	
	@Override
	public boolean disconnect() {
		try {
			mqttClient.disconnect();
		}
		catch(MqttException me) {
			me.printStackTrace();
			return false;
		}
		return true;
	}
	
	private String arrayToJSONStr(boolean[][] array) {
		JSONObject json = new JSONObject();
		json.put("action", "bitmap");
		json.put("x", array[0].length);
		json.put("y", array.length); 
		json.put("map", boolArrayToIntArray(array));
		return json.toString();
	}
	
	private int[][] boolArrayToIntArray(boolean[][] array) {
		int[][] outputArray = new int[array.length][array[0].length];
		
		int i = 0, j = 0;
		for (boolean[] boolArray : array) {
			for (boolean bool : boolArray) {
				outputArray[i][j++] = bool ? 1 : 0;
			}
			j = 0;
			i++;
		}
		
		return outputArray;
	}
	
	private boolean publish(String message) {
		try {
	        MqttMessage mqttMessage = new MqttMessage(message.getBytes());
	        mqttMessage.setQos(qos);
	        mqttClient.publish(topic, mqttMessage);
		}
		catch(MqttException me) {
			me.printStackTrace();
			return false;
		}
		return true;
	}

}
