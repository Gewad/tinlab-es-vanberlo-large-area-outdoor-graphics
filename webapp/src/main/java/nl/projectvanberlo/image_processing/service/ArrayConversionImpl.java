package nl.projectvanberlo.image_processing.service;

import lombok.extern.slf4j.Slf4j;
import nl.projectvanberlo.image_processing.property.ClusterImageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class ArrayConversionImpl implements ArrayConversion {
    private final int clusterSizeDevice;

    @Autowired
    public ArrayConversionImpl(ClusterImageProperties properties) {
        this.clusterSizeDevice = properties.getClusterSize();
    }

    @Override
    public boolean[][] convert(String iP){

        log.info("Starting image bool array conversion");
        boolean[][] array;
        BufferedImage bi;
        try {
            bi = ImageIO.read(new File(iP));
            array = imageToArray(bi);
            array = imagePrep(array);
            log.info("Successfully created the image bool array");
            return array;
        } catch (IOException ioe) {
            log.error("Couldn't read file, invalid file format.");

        } catch (NullPointerException npe) {
            log.error("Unknown source, file format not applicable for conversion.");

        } catch (Exception e) {
            log.error("Unknown error by conversion of input file to bmp format");
            e.printStackTrace();
        }

        return null;
    }

    private boolean[][] imageToArray(BufferedImage image) {
        boolean[][] array = new boolean[0][0];
        array = new boolean[image.getHeight()][image.getWidth()];

        for (int xPixel = 0; xPixel < image.getWidth(); xPixel++) {
            for (int yPixel = 0; yPixel < image.getHeight(); yPixel++) {
                int color = image.getRGB(xPixel, yPixel);

                if (color == Color.BLACK.getRGB()) {
                    array[yPixel][xPixel] = true;

                } else if (color == Color.WHITE.getRGB()) {
                    array[yPixel][xPixel] = false;

                } else {
                    array[yPixel][xPixel] = false;
                }
            }
        }
        return array;
    }

    //This function should be in the api later for expandability
    private boolean[][] imagePrep(boolean[][] array) {
        array = horizontalFlip(array);
        array = verticalFlip(array);
        return array;
    }

    private boolean[][] horizontalFlip(boolean[][] array) {
        for (int arrayYIter = 0; arrayYIter < array.length; arrayYIter++) {
            for (int arrayXIter = 0; arrayXIter < array[0].length; arrayXIter+=clusterSizeDevice) {
                //every other clusterSizeDevice swap the order
                if((arrayXIter % (clusterSizeDevice*2)) > 0) {
                    boolean[] tempArray = new boolean[clusterSizeDevice];
                    for (int i = 0; i < clusterSizeDevice; i++) {
                        tempArray[i] = array[arrayYIter][arrayXIter + i];
                    }
                    int j = clusterSizeDevice-1;
                    for (int i = 0; i < clusterSizeDevice; i++) {
                        array[arrayYIter][arrayXIter + i] = tempArray[j--];
                    }
                }
            }
        }
        return array;
    }

    private boolean[][] verticalFlip(boolean[][] array) {
        for (int arrayXIter = 0; arrayXIter < array[0].length; arrayXIter+=(clusterSizeDevice*2)) {
            boolean[][] tempArray = new boolean[array.length][clusterSizeDevice];
            //Store the column of clusterSizeDevice in width in a temp array
            for (int arrayYIter = 0; arrayYIter < array.length; arrayYIter++) {
                for (int i = 0; i < clusterSizeDevice; i++) {
                    tempArray[arrayYIter][i] = array[arrayYIter][arrayXIter + i];
                }
            }
            //put the temp array in opposite order back in the original array.
            int j = array.length - 1;
            for (int arrayYIter = 0; arrayYIter < array.length; arrayYIter++) {
                for (int i = 0; i < clusterSizeDevice; i++) {
                    array[arrayYIter][arrayXIter + i] = tempArray[j][i];
                }
                j--;
            }
        }
        return array;
    }
}
