package nl.projectvanberlo.image_processing.service;

import java.io.IOException;

public interface ImageConversion {
    /**
     * Converts image and returns construction boolean map for the robot
     *
     * This is the main method of the conversion code
     * First, it will convert the given image to a BMP file format (BMPConversion.java),
     * Second, the image will be made monochrome (MonochromeConversion.java),
     * Third, the image will be clustered to make the pixelsize divisible by the amount of nozzles.
     * Fourth, a construction bitmap will be created based on the image, this will be returned by this method.
     *
     * @param filename The filename and filetype of the image selected, used to find and read the image
     * @return construction boolean map for the robot to paint
     * @throws IOException
     */
    boolean[][] convert(String filename) throws IOException;

    /**
     * Get the output File Name based on the input file name
     * @param filename the filename including file extention of the input file
     * @return a string with the output file name
     */
    String getOutputFileName(String filename);
}