package nl.projectvanberlo.image_processing.service;

public interface BMPConversion {
    /**
     * Converts the image filetype to .bmp
     *
     * The bmp filetype conversion will be done by using base Java imports
     *
     * @param iP inputPath, This is the path where the selected image is located, including its name and filetype
     * @param oP, outputPath, This is the path where the converted image will be saved, including its name and filetype
     */
    void convert(String iP, String oP);
}
