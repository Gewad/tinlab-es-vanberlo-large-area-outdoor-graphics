package nl.projectvanberlo.image_processing.service;

public interface MonochromeConversion {
    /**
     * Overwrites the bmp image to its monochrome version
     */
    void convert(String oP);
}
