package nl.projectvanberlo.image_processing.service;

public interface MqttService {
	/**
	 * Publish the bitmap to the predefined topic.
	 * @param bitmap A 2D array of boolean values with true meaning that there is a pixel and false of there isn't one.
	 * @return True if the bitmap was published successfully else False.
	 */
	boolean sendBitmap(boolean[][] bitmap);

	/**
	 * Connect to the MQTT broker.
	 * @return True if connection was successful else false.
	 */
	boolean connect();

	/**
	 * Disconnect from the MQTT broker.
	 * @return True if disconnection was successful else false.
	 */
	boolean disconnect();
}
