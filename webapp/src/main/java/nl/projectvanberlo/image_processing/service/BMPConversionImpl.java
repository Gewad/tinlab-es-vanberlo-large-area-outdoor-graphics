package nl.projectvanberlo.image_processing.service;

import lombok.extern.slf4j.Slf4j;
import net.sf.image4j.codec.bmp.BMPEncoder;
import nl.projectvanberlo.image_processing.exception.FileConversionException;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Slf4j
@Service
public class BMPConversionImpl implements BMPConversion {
    @Override
    public void convert(String iP, String oP) throws FileConversionException {

        try {
            log.info("Starting image to .bmp conversion");

            BufferedImage bi = ImageIO.read(new File(iP));
            BufferedImage result = filterTransparency(bi, iP);
            BMPEncoder.write(result, new File(oP));

            log.info("Successfully converted image to .bmp");

        } catch (IOException ioe) {
            log.error("Couldn't read file, invalid file format.");

        } catch (NullPointerException npe) {
            log.error("Unknown source, file format not applicable for conversion.");

        } catch (Exception e) {
            log.error("Unknown error by conversion of input file to bmp format");
            e.printStackTrace();
        }

    }

    /**
     * Changes transparent pixels into white pixels
     *
     * Every pixel will be checked if it holds a transparency percentage higher than the given threshold.
     * These pixels are changed to a fully white pixel with this method.
     * If these pixels are not handled, the BMPencoder will automatically make these pixels black, which is not desired.
     * The threshold is tested and used as a local variable.
     *
     * Using a BMP file with transparent pixels throws an exception, as this does not fully function.
     *
     * @param image Any image that might contain transparent pixels.
     * @param iP This input path string is used to match filetypes
     * @return image with transparent pixels filtered out if applicable.
     */
    private BufferedImage filterTransparency(BufferedImage image, String iP){
        double transparencyThreshold = 0.6;
        int threshold = (int) (255*transparencyThreshold);

        BufferedImage result = new BufferedImage(
                image.getWidth(),
                image.getHeight(),
                BufferedImage.TYPE_INT_ARGB);

        int white = (new Color(255,255,255)).getRGB();
        int rgb, alpha;
        for (int heightIter = 0; heightIter < image.getHeight(); heightIter++){
            for (int widthIter = 0; widthIter < image.getWidth(); widthIter++) {
                rgb = image.getRGB(widthIter, heightIter);
                alpha = (rgb & 0xff000000) >>> 24;

                if (alpha <= threshold) {
                    if (iP.endsWith(".bmp"))
                        throw new FileConversionException(".bmp files currently do not support transparency");

                    result.setRGB(widthIter, heightIter, white);
                }else
                    result.setRGB(widthIter, heightIter, rgb);
            }
        }
        return result;
    }
}
