package nl.projectvanberlo.image_processing.service;

import java.awt.image.BufferedImage;
import java.io.IOException;

public interface ClusterImage {
    /**
     * clusters an image by inputting a filepath of the input image file and overwriting it width the clustered image.
     *
     * @param oP Input image filepath
     */
    void cluster(String oP);

    /**
     * Clusters an BufferedImage by making the resolution dividable by the cluster size of the device.
     *
     * This method clusters the image by scaling the image using bicubic interpolation and
     * then cropping the image to the right size.
     *
     * @param image A BufferedImage of the image to cluster.
     * @return A BufferedImage of the clusterd image.
     */
    BufferedImage clusterBufferedImage(BufferedImage image);

    /**
     * Clusters an BufferedImage by making the resolution smaller than the
     * maximum width but still dividable by the cluster size of the device.
     *
     * This method clusters the image by scaling the image using bicubic interpolation and
     * then cropping the image to the right size.
     *
     * @param image A BufferedImage of the image to cluster.
     * @param maxRows Specify the maximum width of image in multiples of clusterSizeDevice.
     * @return A BufferedImage of the clustered image.
     */
    BufferedImage clusterBufferedImage(BufferedImage image, int maxRows);
}
