package nl.projectvanberlo.image_processing.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("storage")
public class ClusterImageProperties {
    private int clusterSize = 5;
}