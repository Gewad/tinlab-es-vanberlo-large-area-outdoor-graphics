package nl.projectvanberlo.image_processing.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Getter
@Setter
@ConfigurationProperties("storage")
public class StorageProperties {
    private String location = "/var/files/";
    private int maxUploadSize = 1000000000; //The maximum size a file can be n bytes
}
