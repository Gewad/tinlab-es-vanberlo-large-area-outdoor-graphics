package nl.projectvanberlo.image_processing.property;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
@Getter
@Setter
@ConfigurationProperties("mqtt")
public class MqttProperties {

	private String clientId = "Image_web_app";
    private String broker = "tcp://localhost:1883";
    private String topic = "bitmap";
    private int qos = 2;
}
