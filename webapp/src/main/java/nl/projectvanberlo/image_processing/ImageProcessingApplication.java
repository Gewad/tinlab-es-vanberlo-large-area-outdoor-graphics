package nl.projectvanberlo.image_processing;

import lombok.extern.slf4j.Slf4j;
import nl.projectvanberlo.image_processing.property.StorageProperties;
import nl.projectvanberlo.image_processing.property.MqttProperties;
import nl.projectvanberlo.image_processing.property.ClusterImageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties({StorageProperties.class, ClusterImageProperties.class, MqttProperties.class})
@Slf4j
public class ImageProcessingApplication {

    public static void main(String[] args) {
        SpringApplication.run(ImageProcessingApplication.class, args);
    }

}
