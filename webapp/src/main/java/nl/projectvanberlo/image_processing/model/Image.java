package nl.projectvanberlo.image_processing.model;

import lombok.Data;

@Data
public class Image {
    private int id;
    private String imagePath;
}
