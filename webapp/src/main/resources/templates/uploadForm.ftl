<html xmlns:th="https://www.thymeleaf.org">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
    <title>Large Outdoor Graphics - VanBerlo</title>
    <meta name="description" content="Large Outdoor Graphics">

    <!-- Favicon -->
    <link rel="image_src" href="/img/logo.png">
    <link rel="shortcut icon" href="/img/logo.png">
    <link rel="apple-touch-icon" href="/img/logo.png">

	<!-- Fonts & Icons -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,900|Roboto+Mono&display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

    <!-- CSS -->
    <link href="/css/materialize.min.css" type="text/css" rel="stylesheet" media="screen,projection"/>
    <link href="/css/main.css" type="text/css" rel="stylesheet"/>
</head>
<body>
    <!-- Navbar -->
    <nav class="black" role="navigation">
        <div class="nav-wrapper container">
            <a id="logo-container" href="/" class="brand-logo">VAN<b>BERLO</b></a>
            <ul class="right hide-on-med-and-down mono">
                <!-- Navbar entries desktop -->
                <li><a href="/">Home</a></li>
                <li><a href="#">Information</a></li>
                <li><a href="#">Manual</a></li>
            </ul>
            <ul id="nav-mobile" class="sidenav">
                <li><a class="menu sidenav-close noselect"><i class="material-icons">close</i>Menu</a></li>
                <li><div class="divider"></div></li>
                <!-- Navbar entries mobile -->
                <li><a href="/"><i class="material-icons">home</i>Home</a></li>
                <li><a href="#"><i class="material-icons">info</i>Information</a></li>
                <li><a href="#"><i class="material-icons">import_contacts</i>Manual</a></li>
            </ul>
            <a data-target="nav-mobile" class="sidenav-trigger right"><i class="material-icons">menu</i></a>
        </div>
    </nav>
    <!-- End Navbar -->

    <!-- Content Start -->
    <main>
        <!-- Info Banner -->
        <div class="section no-pad-bot">
            <div class="container">
                <br><br>
                <h1 class="header red-text text-accent-2">Large Outdoor Graphics</h1>
                <h5><blockquote class="grey-text text-darken-2">Start printing your graphics today</blockquote></h5>
                <br><br>
            </div>
        </div>
        <!-- End Info Banner -->

        <!-- Upload Section -->
        <div class="section no-pad-bot">
            <div class="container">
                <!-- Upload form -->
                <div class="row">
                    <h4>Upload Graphic</h4>

                    <!-- Preview Image -->
                    <img id="imageToLoad" src="#" alt=" " style="margin-bottom: 10px">

                    <form onsubmit="return Validate(this);" method="POST" enctype="multipart/form-data" action="/">
                        <label class="btn waves-effect waves-light red accent-2">
                            <input type="file" name="file" onchange="readURL(this);" />
                            Select File
                        </label>
                        <label class="btn waves-effect waves-light red accent-2">
                            <input type="submit" value="Upload" />
                            Upload File
                        </label>
                    </form>

                    <!-- Messages -->
                    <div class="row">
                        <#if message??>
                            <p><blockquote class="grey-text text-darken-2">${message}</blockquote></p>
                        </#if>
                    </div>
                    <br>
                </div>
            </div>
        </div>
        <!-- End Upload Section -->

        <!-- List Files Section -->
        <div class="section">
            <div class="container">
                <div class="row">
                    <h4>Graphics</h4>
                    <!-- TODO check if there are files -->
                    <ul class="collection">
                        <#list files as file>
                        <a href="${file}" class="collection-item avatar">
                            <img src=${file} alt="" class="circle">
                            <span class="black-text mono">${file}</span>
                            <span class="secondary-content red-text text-accent-2" style="margin-left: 20px"><i class="material-icons">get_app</i></span>
                        </a>
                        </#list>
                    </ul>
                    <!-- TODO end files check -->
                </div>
            </div>
        </div>
        <!-- End List Files Section -->
    </main>
    <!-- Content End -->

    <!-- Footer -->
    <footer class="page-footer red accent-2">
        <div class="container">
            <div class="row">
                <h5 class="white-text">Teams</h5>
                <div class="col l4 s12">
                    <h6 class="white-text mono bold">Team Painting</h6>
                    <ul>
                        <li>&gt; Thomas Heus</li>
                        <li>&gt; Paul de Hek</li>
                        <li>&gt; Damian bachasingh</li>
                        <li>&gt; Quentin Hoogwerf</li>
                    </ul>
                </div>
                <div class="col l4 s12">
                    <h6 class="white-text mono bold">Team Mechanics</h6>
                    <ul>
                        <li>&gt; Jacco Troost</li>
                        <li>&gt; Lex van Teeffelen</li>
                        <li>&gt; Gerard van Walraven</li>
                        <li>&gt; Paul Wondel</li>
                    </ul>
                </div>
                <div class="col l4 s12">
                    <h6 class="white-text mono bold">Team Graphics</h6>
                    <ul>
                        <li>&gt; Bryan van Dijk</li>
                        <li>&gt; Max van Meijeren</li>
                        <li>&gt; Ramon Rooijens</li>
                    </ul>
                </div>
            </div>
        </div>
        <br>
        <div class="footer-copyright">
            <div class="container">
                &copy; <a class="white-text" target="_blank" href="https://vanberloagency.com/">Van<b>Berlo</b> Part of Accenture Industry X.O 2020</a>
            </div>
        </div>
    </footer>
    <!-- End Footer -->

    <!-- Scripts (do not change load order) -->
    <script src="/js/main.js"></script>
    <script src="/js/jquery.min.js"></script>
    <script src="/js/materialize.min.js"></script>
    <script src="/js/init.js"></script>
</body>
</html>
