// Validate file input
var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".svg", ".png", ".eps"];
function Validate(oForm) {
    var arrInputs = oForm.getElementsByTagName("input");
    for (var i = 0; i < arrInputs.length; i++) {
        var oInput = arrInputs[i];
        if (oInput.type == "file") {
            var sFileName = oInput.value;
            if (sFileName.length > 0) {
                var blnValid = false;
                for (var j = 0; j < _validFileExtensions.length; j++) {
                    var sCurExtension = _validFileExtensions[j];
                    if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                        blnValid = true;
                        break;
                    }
                }

                if (!blnValid) {
                    alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", "));
                    return false;
                }
            }
        }
    }
    return true;
}

// Show Preview image after selecting
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader(); // Only supported in HTML5??
        reader.onload = function (e) {
            $('#imageToLoad').attr('src', e.target.result).width(200)
        };
        reader.readAsDataURL(input.files[0]);
    }
}
