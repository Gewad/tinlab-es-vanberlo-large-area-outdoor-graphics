# TinLab-ES - VanBerlo - Large-Area Outdoor Graphics

Project for TinLab Embedded Systems and VanBerlo.

# Web Application User Manual

## Installation
Make sure the following is installed on your system
- Mosquitto
- Java

Create the folder /var/files

## Editing the settings
To edit settings of the server you can chance the properties 
in the properties folder.

`src/main/java/nl/projectvanberlo/image_processing/property/`

## Building the project
You can build the project with gradle.

`gradle clean build`

## Running the web application
To run the web application first start the mosquitto broker.

`$ mosquitto`

Now you can start the web application.

`$ java -jar build/libs/image_processing-0.1.jar`

## Uploading an image
Go to `localhost:80` in you web browser. Here you can select an image and upload it if you want. 
When you click upload the image will be converted to a bitmap and send away via MQTT.

## Output server
The server sends a json string over MQTT in the topic `bitmap`.
The json string contains:
```
{
    action: "bitmap",
    y: [length of bitmap],
    x: [width of bitmap],
    map: [bool[][] array in 1 and 0's]
}
```

